# Comprimir Imagenes

Este repositorio contiene el codígo para comprimir imagenes sin perder la calidad.

- [Acerca de](#acerca-de)
- [Recursos](#recursos)


## Acerca de 

El codígo fue generado para optimizar y reducir el peso de las imagenes antes de ser alamacenadas en un servidor.


### Recursos

Para poder realizar la funcionalidad de comprimir los archivos pdf, feron necesarios varios recursos como por ejemplo. 

* [Bootstrap v5.0](https://getbootstrap.com/)
* [Compressor.js](https://fengyuanchen.github.io/compressorjs)

![demo](public/docs/captura.png)

En la siguiente liga se muestra el [ejemplo](https://salvatorelopez.gitlab.io/comprimir-imagenes/) funcional.