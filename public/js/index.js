var URL = window.URL || window.webkitURL;
var imagen_entrada = undefined;
var imagen_salida = undefined;
const imagen_sin_comprimir = document.querySelector(".imagen-sin-comprimir");
const detalle_imagen_sin_comprimir = document.querySelector(".detalle-imagen-sin-comprimir");
const imagen_comprimida = document.querySelector(".imagen-comprimida");
const detalle_imagen_comprimida = document.querySelector(".detalle-imagen-comprimida");
const descargar_imagen_sin_comprimir = document.querySelector(".descargar-imagen-sin-comprimir");
const descargar_imagen_comprimida = document.querySelector(".descargar-imagen-comprimida");
const buscar_imagen = document.querySelector(".buscar-imagen");


var imprimir_detalles = (imagen) => {
    return `<dl class="row">
        <dt class="col-5">Nombre:</dt>
        <dd class="col-7">${imagen.name}</dd>
        <dt class="col-5">Tipo:</dt>
        <dd class="col-7">${imagen.type}</dd>
        <dt class="col-5">Tamaño:</dt>
        <dd class="col-7">${convertir_peso(imagen.size)}</span>
        </dd>
    </dl>`
}

var convertir_peso = (peso_imagen) => {
    const kilobyte = 1024;
    const megabyte = kilobyte * kilobyte;

    if (peso_imagen > megabyte) {
        return (peso_imagen / megabyte).toFixed(2) + ' MB';
    } else if (peso_imagen > kilobyte) {
        return (peso_imagen / kilobyte).toFixed(2) + ' KB';
    } else if (peso_imagen >= 0) {
        return peso_imagen + ' B';
    }
    return 'N/A';
}

var comprimir = (imagen) => {

    if (!imagen) {
        return;
    }

    console.log('imagen de entrada: ', imagen);

    if (URL) {
        imagen_entrada = URL.createObjectURL(imagen);
        imagen_sin_comprimir.setAttribute('src', imagen_entrada);
        descargar_imagen_sin_comprimir.setAttribute('href', imagen_entrada);
        detalle_imagen_sin_comprimir.innerHTML = imprimir_detalles(imagen);
    }

    new Compressor(imagen, {
        quality: 0.6,
        success(result) {
            console.log('imagen de salida: ', result);
            result.name =  result.name == undefined ? "imagen.jpg": result.name;
            imagen_salida = URL.createObjectURL(result);
            imagen_comprimida.setAttribute('src', imagen_salida);
            descargar_imagen_comprimida.setAttribute('href', imagen_salida);
            detalle_imagen_comprimida.innerHTML = imprimir_detalles(result);

        },
        error(err) {
            console.log(err.message);
        },
    });
}


buscar_imagen.onchange = (e) => {
    imagen = e.target.files[0];
    if (!imagen) {
        return;
    }
    comprimir(imagen);
}

fetch("img/imagen.jpg")
    .then((response) => response.blob())
    .then((data) => {
        let imagen = data;
        imagen.name = "imagen.jpg";
        comprimir(imagen);
    });